#include <iostream>
#include "Head.hpp"

namespace cl
{

    rectangle::rectangle(float a , float b)
    {
        if( a > 0 )
            m_a = a;
        else
        {
            std::cout<<"Число 'a' должно быть больше нуля, автоматическая замена числа 'a' на 1"<<std::endl;
            m_a = 1;
        }

        if( b >  0 )
            m_b = b;
        else
        {
            std::cout<<"Число 'b' должно быть больше нуля, автоматическая замена числа b' на 1"<<std::endl;
            m_b = 1;
        }
    }
    rectangle::~rectangle()
    {
    }

    float rectangle::S()
    {
        float Ss;
        Ss = m_b * m_a;
        return Ss;
    }
    float rectangle::P()
    {
        float Pp;
        Pp = 2*m_a + 2*m_b;
        return Pp;
    }

}





