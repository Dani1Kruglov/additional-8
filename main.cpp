#include <iostream>
#include "Head.hpp"





int main()
{
    float a, b;

    std::cout << "Введите длину прямоугольника ( a > 0): " << std::endl;
    std::cin >> a;
    std::cout << "Введите ширину прямоугольника (b > 0): " << std::endl;
    std::cin >> b;

    cl::rectangle Pr(a,b);
    std::cout << "Площадь прямоугольника: " << Pr.S()  <<std::endl<< "Периметр прямоугольника: " << Pr.P() << std::endl;

    return 0;
}

