
#ifndef PR2_HEAD_HPP
#define PR2_HEAD_HPP

#endif
#pragma once
#include <iostream>


namespace cl
{
    class rectangle
    {
    public:

        float S();
        float P();

        rectangle(float a , float b);

        ~rectangle();

    private:
        float m_a,m_b;
    };
}
